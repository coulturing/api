import mongoose from 'mongoose';
import logger from '../model/logger';
import {mongodbUri} from '../../constants/string';

mongoose.connect(mongodbUri);

const db = mongoose.connection;

db.on('error', (err) => {
  logger.error('Connection error:', err.message);
});

db.once('open', () => {
  logger.warn("Cononnected to DB!");
});

export default mongoose;