import express from  'express';
import logger from '../model/logger';
import jwtauth from '../auth/jwtauth';
import Task from '../model/task';
import sendData from '../model/sendData';

const router = express.Router();

//获取全部task
router.get('/', jwtauth, (req, res) => {
  Task.find((err, tasks) => {
    if (!err) {
      return res.json(sendData(tasks));
    } else {
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.sendStatus(500);
    }
  });
});

//创建task
router.post('/', (req, res) => {
  const task = new Task({
    title: req.body.title,
    author: req.body.author,
    content: req.body.content,
    date: req.body.date,
    grade: req.body.grade,
  });
  task.save((err, data) => {
    if (!err) {
      logger.info("New task created with id: %s", task.id);
      return res.json(data);
    } else {
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      return err.name === 'ValidationError' ? res.sendStatus(400) : res.sendStatus(500);
    }
  });
});

//获取某一个task
router.get('/:id', (req, res) => {
  Task.findById(req.params.id, (err, task)=> {
    if (!err) {
      return res.json(task);
    }
    else {
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.sendStatus(500);
    }
  });
});

//修改某个task内容
// router.patch('/:id',  (req, res) =>{
//     const taskId = req.params.id;
//     Task.findById(taskId,  (err, task)=> {
//       task.title = req.body.title;
//       task.description = req.body.description;
//       task.author = req.body.author;
//       task.images = req.body.images;
//
//       task.save((err) =>{
//         if (!err) {
//           logger.info("Task with id: %s updated", task.id);
//           return res.json({
//             status: 'OK',
//             task: task
//           });
//         } else {
//           if (err.name === 'ValidationError') {
//             res.statusCode = 400;
//             return res.json({
//               error: 'Validation error'
//             });
//           } else {
//             res.statusCode = 500;
//
//             return res.json({
//               error: 'Server error'
//             });
//           }
//           // logger.error('Internal error (%d): %s',res.statusCode, err.message);
//         }
//       });
//     });
//   });

module.exports = router;