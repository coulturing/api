import express from  'express';

import logger from '../model/logger';
import jwtauth from '../auth/jwtauth';
import PrivateMemo from '../model/privateMemo';
import PublicMemo from  '../model/publicMemo';
import sendData from '../model/sendData';

const router = express.Router();


router.all('*', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  res.header("X-Powered-By", ' 3.2.1');
  res.header("Content-Type", "application/json;charset=utf-8");
  req.method == "OPTIONS" ? res.send(200) : next();
});

router.get('/public', /*jwtauth,*/ (req, res) => {
  PublicMemo.find((err, data) => {
    if (!err) {
      return res.json(sendData(data));
    }
    else {
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.sendStatus(500);
    }
  });
});

router.get('/private', jwtauth, (req, res) => {
  PrivateMemo.find({ userId: req.userId }, (err, data) => {
    if (!err) {
      return res.json(sendData(data));
    } else {
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.sendStatus(500);
    }
  });
});

router.post('/public', jwtauth, (req, res) => {
  const memo = new PublicMemo({
    title: req.body.title,
    content: req.body.content,
    date: req.body.date,
    grade: req.body.grade,
    author: req.body.author
  });
  memo.save((err, data) => {
    if (!err) {
      logger.verbose("New memo created with id: %s", data.id);
      return res.json(data);
    }
    else {
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      if (err.name === 'ValidationError') {
        return res.sendStatus(400);
      }
      else {
        return res.sendStatus(500);
      }
    }
  });
});

router.post('/private', jwtauth, (req, res) => {
  const memo = new PrivateMemo({
    title: req.body.title,
    content: req.body.content,
    date: req.body.date,
    userId: req.body.userId,
    grade: req.body.grade
  });
  memo.save((err, data) => {
    if (!err) {
      logger.verbose("New memo created with id: %s", memo.id);
      return res.json(memo);
    }
    else {
      if (err.name === 'ValidationError') {
        res.statusCode = 400;
        res.json({
          error: 'Validation error'
        });
      }
      else {
        res.statusCode = 500;
        res.json({
          error: 'Server error'
        });
      }
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
    }
  });
});

router.get('/public/:id', (req, res) => {
  PublicMemo.findById(req.params.id, (err, memo) => {
    if (!err) {
      return !memo ? res.json('Not found') : res.json(memo);
    }
    else {
      res.statusCode = 500;
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.json({
        error: 'Server error'
      });
    }
  });
});

router.get('/private/:id', (req, res) => {
  console.log('userId :' + req.body.userId);
  PublicMemo.findById(req.params.id, (err, memo) => {
    if (!memo) {
      return res.json(null);
    }
    if (!err) {
      return res.json(memo);
    }
    else {
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.sendStatus(500);
    }
  });
});

router.patch('/public/:id', jwtauth, (req, res) => {
  const memoId = req.params.id;
  PublicMemo.findById(memoId, (err, memo) => {
    if (!memo) {
      logger.error('memo with id: %s Not Found', memoId);
      return res.sendStatus(500);
    }

    memo.title = req.body.title;
    memo.content = req.body.content;
    memo.date = req.body.date;
    memo.grade = req.body.grade;
    PublicMemo.update({ _id: memoId }, memo, (err) => {
      if (!err) {
        logger.info("memo with id: %s updated", memo.id);
        return res.json(memo);
      } else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          return res.json({
            error: 'Validation error'
          });
        } else {
          res.statusCode = 500;

          return res.json({
            error: 'Server error'
          });
        }
      }
    });
  });
});

router.patch('/private/:id', jwtauth, (req, res) => {
  var memoId = req.params.id;
  PublicMemo.findById(memoId, (err, memo) => {
    if (!memo) {
      res.statusCode = 404;
      logger.error('memo with id: %s Not Found', memoId);
      return res.json({
        error: 'Not found'
      });
    }
    memo.title = req.body.title;
    memo.content = req.body.content;
    memo.date = req.body.date;
    memo.userId = req.body.userId;
    memo.grade = req.body.grade;
    PrivateMemo.update({ _id: memoId }, memo, (err) => {
      if (!err) {
        logger.info("memo with id: %s updated", memo.id);
        return res.json(memo);
      } else {
        if (err.name === 'ValidationError') {
          res.statusCode = 400;
          return res.json({
            error: 'Validation error'
          });
        } else {
          res.statusCode = 500;

          return res.json({
            error: 'Server error'
          });
        }
      }
    });
  });
});
router.delete('/public/:id', jwtauth, (req, res) => {
  PublicMemo.remove({ _id: req.params.id }, (err, memo) => {
    if (!memo) {
      res.statusCode = 404;

      return res.json({
        error: 'Not found'
      });
    }
    if (!err) {
      logger.verbose('delete public memo success ' + req.params.id);
      return res.json({ status: 200 });
    } else {
      res.statusCode = 500;
      logger.error('Internal error(%d): %s', res.statusCode, err.message);

      return res.json({
        error: 'Server error'
      });
    }
  });
});
router.delete('/private/:id', jwtauth, (req, res) => {
  PublicMemo.remove({ _id: req.params.id }, (err, memo) => {
    if (!memo) {
      res.statusCode = 404;
      return res.json({
        error: 'Not found'
      });
    }
    if (!err) {
      logger.verbose('delete private memo success ' + req.params.id);
      return res.json({ status: 200 });
    }
    else {
      res.statusCode = 500;
      logger.error('Internal error(%d): %s', res.statusCode, err.message);
      return res.json({
        error: 'Server error'
      });
    }
  });
});

module.exports = router;
