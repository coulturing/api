import express from 'express';
const router = express.Router();


router.all('*', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  res.header("X-Powered-By", ' 3.2.1');
  res.header("Content-Type", "application/json;charset=utf-8");
  req.method == "OPTIONS" ? res.send(200) : next();
});

/* GET users listing. */
router.get('/yystlx', /*passport.authenticate('bearer', { session: false }), */function (req, res) {
    res.json({
        A: ['selections', '完形填空'],
        B: ['blanks', '写作'],
        C: ['selections', '阅读理解'],
        D: ['blanks', '翻译'],
        E: ['blanks', '听力理解'],
        F: ['blanks', '听力短文'],
        G: ['blanks', '听力填空'],
        H: ['blanks', '短文填空'],
        I: ['blanks', '选词填空'],
        J: ['blanks', '段落填空'],
        K: ['options', '段落定位'],
    });
});
router.get('/', /*passport.authenticate('bearer', { session: false }), */function (req, res) {
    res.json({msg: 'erdtfyguhjikldfgh'});
});

module.exports = router;
