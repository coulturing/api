import express from 'express';

const router = express.Router();

router.get('/info', (req, res) =>{
        res.json({
        	user_id: req.user.userId, 
        	name: req.user.username, 
        	scope: req.authInfo.scope 
        });
    }
);

module.exports = router;