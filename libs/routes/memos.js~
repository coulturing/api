var express = require('express');
var passport = require('passport');
var router = express.Router();

var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);

var db = require(libs + 'db/mongoose');
var PrivateMemo = require(libs + 'model/privateMemo');
var PublicMemo = require(libs + 'model/publicMemo');

router.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1');
    res.header("Content-Type", "application/json;charset=utf-8");
    if (req.method == "OPTIONS")
        res.send(200);/*让options请求快速返回*/
    else  next();
});
router.get('/public', // passport.authenticate('bearer', { session: false }),
    function (req, res) {
        console.log('user ip:' + req.ip);

        PublicMemo.find(function (err, data) {

            if (!err) {
                var sendData = {};
                sendData.status = 200;
                sendData.count = data.length;
                sendData.previous = null;
                sendData.results = data;
                return res.json(
                    sendData);


            } else {
                res.statusCode = 500;

                log.error('Internal error(%d): %s', res.statusCode, err.message);

                return res.json({
                    error: 'Server error'
                });
            }
        });
    });

router.get('/private', // passport.authenticate('bearer', { session: false }),
    function (req, res) {
        console.log('user ID:' + req.userId);
        console.log('user ip:' + req.ip);

        PrivateMemo.find({userId: req.userId}, function (err, data) {

            if (!err) {
                var sendData = {};
                sendData.status = 200;
                sendData.count = data.length;
                sendData.previous = null;
                sendData.results = data;
                return res.json(
                    sendData);


            } else {
                res.statusCode = 500;

                log.error('Internal error(%d): %s', res.statusCode, err.message);

                return res.json({
                    error: 'Server error'
                });
            }
        });
    });

router.post('/public', // passport.authenticate('bearer', { session: false }),
    function (req, res) {

        var memo = new PublicMemo({
            title: req.body.title,
            content: req.body.content,
            date: req.body.date,
            grade: req.body.grade,
	    author:req.body.author
        });

        memo.save(function (err) {
            if (!err) {
                log.info("New memo created with id: %s", memo.id);
                return res.json(memo);
            } else {
                if (err.name === 'ValidationError') {
                    res.statusCode = 400;
                    res.json({
                        error: 'Validation error'
                    });
                } else {
                    res.statusCode = 500;
                    res.json({
                        error: 'Server error'
                    });
                }
                log.error('Internal error(%d): %s', res.statusCode, err.message);
            }
        });
    });


router.post('/private', // passport.authenticate('bearer', { session: false }),
    function (req, res) {

        var memo = new PrivateMemo({
            title: req.body.title,
            content: req.body.content,
            date: req.body.date,
            userId: req.body.userId,
            grade: req.body.grade
        });

        memo.save(function (err) {
            if (!err) {
                log.info("New memo created with id: %s", memo.id);
                return res.json(memo);
            } else {
                if (err.name === 'ValidationError') {
                    res.statusCode = 400;
                    res.json({
                        error: 'Validation error'
                    });
                } else {
                    res.statusCode = 500;
                    res.json({
                        error: 'Server error'
                    });
                }
                log.error('Internal error(%d): %s', res.statusCode, err.message);
            }
        });
    });

router.get('/public/:id',  //passport.authenticate('bearer', { session: false }),
    function (req, res) {

        PublicMemo.findById(req.params.id, function (err, memo) {

            if (!memo) {
                res.statusCode = 404;

                return res.json({
                    error: 'Not found'
                });
            }

            if (!err) {
                return res.json(memo);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);

                return res.json({
                    error: 'Server error'
                });
            }
        });
    });

router.get('/private/:id',  //passport.authenticate('bearer', { session: false }),
    function (req, res) {
        console.log('userId :' + req.body.userId);
        PublicMemo.findById(req.params.id, function (err, memo) {

            if (!memo) {
                res.statusCode = 404;

                return res.json({
                    error: 'Not found'
                });
            }

            if (!err) {
                return res.json(memo);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);

                return res.json({
                    error: 'Server error'
                });
            }
        });
    });

router.patch('/public/:id', // passport.authenticate('bearer', { session: false }),
    function (req, res) {
        var memoId = req.params.id;

        PublicMemo.findById(memoId, function (err, memo) {
            if (!memo) {
                res.statusCode = 404;
                log.error('memo with id: %s Not Found', memoId);
                return res.json({
                    error: 'Not found'
                });
            }

            memo.title = req.body.title;
            memo.content = req.body.content;
            memo.date = req.body.date;
            memo.grade = req.body.grade;
            PublicMemo.update({_id:memoId},memo,function (err) {
                if (!err) {
                    log.info("memo with id: %s updated", memo.id);
                    return res.json(memo);
                } else {
                    if (err.name === 'ValidationError') {
                        res.statusCode = 400;
                        return res.json({
                            error: 'Validation error'
                        });
                    } else {
                        res.statusCode = 500;

                        return res.json({
                            error: 'Server error'
                        });
                    }
                }
            });
        });
    });

router.patch('/private/:id', // passport.authenticate('bearer', { session: false }),
    function (req, res) {
        var memoId = req.params.id;

        PublicMemo.findById(memoId, function (err, memo) {
            if (!memo) {
                res.statusCode = 404;
                log.error('memo with id: %s Not Found', memoId);
                return res.json({
                    error: 'Not found'
                });
            }
            memo.title = req.body.title;
            memo.content = req.body.content;
            memo.date = req.body.date;
            memo.userId = req.body.userId;
            memo.grade = req.body.grade;
            PrivateMemo.update({_id:memoId},memo,function (err) {
                if (!err) {
                    log.info("memo with id: %s updated", memo.id);
                    return res.json(memo);
                } else {
                    if (err.name === 'ValidationError') {
                        res.statusCode = 400;
                        return res.json({
                            error: 'Validation error'
                        });
                    } else {
                        res.statusCode = 500;

                        return res.json({
                            error: 'Server error'
                        });
                    }
                }
            });
        });
    });
router.delete('/public/:id',  //passport.authenticate('bearer', { session: false }),
    function (req, res) {
        PublicMemo.remove({_id: req.params.id}, function (err, memo) {

            if (!memo) {
                res.statusCode = 404;

                return res.json({
                    error: 'Not found'
                });
            }

            if (!err) {
                return res.json({status: 200});
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);

                return res.json({
                    error: 'Server error'
                });
            }
        });
    });
router.delete('/private/:id',  //passport.authenticate('bearer', { session: false }),
    function (req, res) {
        PublicMemo.remove({_id: req.params.id}, function (err, memo) {

            if (!memo) {
                res.statusCode = 404;

                return res.json({
                    error: 'Not found'
                });
            }

            if (!err) {
                return res.json({status: 200});
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);

                return res.json({
                    error: 'Server error'
                });
            }
        });
    });

module.exports = router;
