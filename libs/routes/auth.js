import express from 'express';
import jwt from 'jwt-simple';
import moment from 'moment';
import logger from '../model/logger';
import AccessToken from '../model/accessToken';
import jwtKey from '../../constants/jwtKey';

const router = express.Router();
router.all('*', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  res.header("X-Powered-By", ' 3.2.1');
  res.header("Content-Type", "application/json;charset=utf-8");
  req.method == "OPTIONS" ? res.send(200) : next();
});

router.get('/', (req, res) => {
  res.send({ msg: 'auth' });
});

router.post('/', (req, res) => {
  console.log(req.body.username);
  if (req.body.username && req.body.password) {
    AccessToken.findOne({ username: req.body.username, password: req.body.password },  (err, data)=> {
      if (!err) {
        if (data) {
          const expires = moment().add(1, 'days').valueOf();
          const token = jwt.encode({
            iss: data.username,
            exp: expires
          }, jwtKey);
          return res.json({
            token: token
          });
        }
        else {
          return res.sendStatus(401);
        }
      }
      else {
        logger.error('Internal error(%d): %s', res.statusCode, err.message);
        return res.sendStatus(500);
      }
    });
  }
  else {
    logger.error('Internal error(%d): %s', res.statusCode, err.message);
    return res.sendStatus(500);
  }
});

module.exports = router;
