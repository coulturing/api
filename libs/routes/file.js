﻿import express from 'express';
var router = express.Router();
var libs = process.cwd() + '/libs/';
var formidable = require('formidable');
var fs = require('fs');

var FileList = require(libs + 'model/fileList');
var AVATAR_UPLOAD_FOLDER = '/avatar/';

router.all('*', (req, res, next)=> {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1');
    res.header("Content-Type", "application/json;charset=utf-8");
    if (req.method == "OPTIONS")
        res.send(200);/*让options请求快速返回*/
    else  next();
});

router.get('/', (req, res) =>{
        FileList.find((err, data)=> {
            if (!err) {
                var sendData = {};
                sendData.status = 200;
                sendData.count = data.length;
                sendData.previous = null;
                sendData.results = data;
                return res.json(
                    sendData);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.json({
                    error: 'Server error'
                });
            }
        });
    });

router.post('/', (req, res) =>{

    var form = new formidable.IncomingForm();   //创建上传表单
    form.encoding = 'utf-8';		//设置编辑
    form.uploadDir = 'public' + AVATAR_UPLOAD_FOLDER;	 //设置上传目录
    form.keepExtensions = true;	 //保留后缀
    form.maxFieldsSize = 2 * 1024 * 1024;   //文件大小

    form.parse(req, (err, fields, files)=> {

        if (err) {
            res.locals.error = err;
            res.render('index', {title: TITLE});
            return;
        }
        var extName = '';  //后缀名
        switch (files.fulAvatar.type) {
            case 'image/pjpeg':
                extName = 'jpg';
                break;
            case 'image/jpeg':
                extName = 'jpg';
                break;
            case 'image/png':
                extName = 'png';
                break;
            case 'image/x-png':
                extName = 'png';
                break;
        }

        if (extName.length == 0) {
            res.locals.error = '只支持png和jpg格式图片';
            res.json({msf: "只支持png和jpg格式图片"});
            return;
        }

        var avatarName = Math.random() + '.' + extName;
        var newPath = form.uploadDir + avatarName;

        console.log(newPath);
        fs.renameSync(files.fulAvatar.path, newPath);  //重命名
    });

    res.locals.success = '上传成功';
    res.json({msg: "上传成功"});
});

module.exports = router;
