import mongoose from 'mongoose';
import { Schema } from 'mongoose';

const AccessToken = new Schema(
    {
        username: {type: String, required: true, index: {unique: true}},
        password: {type: String, required: true},
        grade: {type: Number, required: true},
        name: {type: String, required: true},
        modified: {type: Date, default: Date.now}
    },
    {collection: "AccessToken"}
);

export default mongoose.model('AccessToken', AccessToken);