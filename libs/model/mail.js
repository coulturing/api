import {api_key, domain, monitorMail } from '../../constants/string';
var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });
import logger from './logger';


const sendMail = (subject, html) => {
  const data = {
    from: 'Excited User <postmaster@sandbox0f3e7462e8ba4274a83d05a363f1e63f.mailgun.org>',
    to: monitorMail,
    subject: subject,
    html: html
  };

  mailgun.messages().send(data,  (error) =>{
    return !error ? logger.warn('send mail succ') : logger.error(error);
  });
};
export default sendMail;

