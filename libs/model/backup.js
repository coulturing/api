import fs from 'fs';
import path from 'path';
import schedule from 'node-schedule';
import sendMail from  './mail';
import { backupTime } from '../../constants/string';

const libs = process.cwd() + '/libs/';

const readFileList = (fileName) => {
  const fileUri = path.join(process.cwd(), '/logs/', fileName);
  return fs.readFileSync(fileUri, 'utf-8', (err) =>{
    if (err) {
      throw err;
    }
  });
};

const scheduleCronstyle = () => {
  const html =
    '<h3>info</h3><p>' +
    readFileList('/info.log') +
    '</p><h3>error</h3><p>' +
    readFileList('/error.log') +
    '</p><h3>warn</h3><p>' +
    readFileList('/warn.log') +
    '</p><h3>verbose</h3><p>' +
    readFileList('/verbose.log') +
    '</p>';

  schedule.scheduleJob(backupTime, () =>{
    sendMail('api logs', html);
  });
};

export default scheduleCronstyle;