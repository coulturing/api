import mongoose from 'mongoose';
import {Schema} from  'mongoose';

const ObjectId = Schema.ObjectId;
const Task = new Schema(
  {
    id:ObjectId,
    title: { type: String, required: true },
    comment: { type: Array },
    content: { type: String},
    date: { type: String, required: true },
    grade: { type: String, required: true },
    author: { type: String, required: true },
    modified: { type: Date, default: Date.now }
  },
  { collection: "Task" }
);

Task.path('title').validate((v) =>{
  return v.length > 0 && v.length < 70;
});

export default mongoose.model('Task', Task);