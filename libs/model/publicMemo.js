import mongoose from 'mongoose';
import {Schema} from 'mongoose';

const ObjectId = Schema.ObjectId;
const PublicMemo = new Schema(
  {
    id: ObjectId,
    title: { type: String, required: true },
    content: { type: String, required: true },
    date: { type: String, required: true },
    grade: { type: Number, required: true },
    author: { type: String, required: true },
    modified: { type: Date, default: Date.now }
  },
  { collection: "PublicMemo" }
);

PublicMemo.path('title').validate( (v) =>{
  return v.length > 0 && v.length < 70;
});

export default mongoose.model('PublicMemo', PublicMemo);
