import mongoose from 'mongoose';
import {Schema} from 'mongoose';

// Memo

const PrivateMemo = new Schema(
  {
    title: { type: String, required: true },
    content: { type: String, required: true },
    date: { type: String, required: true },
    grade: { type: String, required: true },
    userId: { type: String, required: true },
    modified: { type: Date, default: Date.now }
  },
  { collection: "PrivateMemo" }
);

PrivateMemo.path('title').validate( (v) =>{
  return v.length > 0 && v.length < 70;
});

export default mongoose.model('PrivateMemo', PrivateMemo);