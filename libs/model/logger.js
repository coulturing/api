var winston = require('winston');

// winston.emitErrs = true;

var logger = new (winston.Logger)({
  transports: [
    new winston.transports.Console({
      colorize: true
    }),
    new (winston.transports.File)({
      name: 'info',
      filename: process.cwd() + '/logs/info.log',
      level: 'info'
    }),
    new (winston.transports.File)({
      name: 'verbose',
      filename: process.cwd() + '/logs/verbose.log',
      level: 'verbose'
    }),
    new (winston.transports.File)({
      name: 'warn',
      filename: process.cwd() + '/logs/warn.log',
      level: 'warn'
    }),
    new (winston.transports.File)({
      name: 'error',
      filename: process.cwd() + '/logs/error.log',
      level: 'error'
    })
  ]
});

module.exports = logger;
