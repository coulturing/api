const sendData = (data) => {
  let sendData = {};
  sendData.status = 200;
  sendData.count = data.length;
  sendData.previous = null;
  sendData.results = data;
  return sendData;
};
export default sendData;