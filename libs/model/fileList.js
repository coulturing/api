import mongoose from 'mongoose';
import { Schema } from 'mongoose';

const FileList = new Schema(
    {
        username: {type: String, required: true, index: {unique: true}},
        uri: {type: String, required: true, index: {unique: true}},
        author: {type: String, required: true, index: {unique: true}},
        category: {type: String, required: true, index: {unique: true}},
        modified: {type: Date, default: Date.now}
    },
    {collection: "FileList"}
);

export default mongoose.model('FileList', FileList);