import mongoose from 'mongoose';
import { Schema } from 'mongoose';

	Client = new Schema({
		name: {
			type: String,
			unique: true,
			required: true
		},
		clientId: {
			type: String,
			unique: true,
			required: true
		},
		clientSecret: {
			type: String,
			required: true
		}
	});

export default mongoose.model('Client', Client);
