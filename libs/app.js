import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';

import config from './config';
import logger from './model/logger';
import scheduleCronstyle from './model/backup';
import api from './routes/api';
import users from './routes/users';
import tasks from './routes/tasks';
import memos from './routes/memos';
import test from './routes/test';
import file from './routes/file';
import auth from './routes/auth';

const app = express();
const libs = process.cwd() + '/libs/';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(methodOverride());
app.use((req, res, next) => {
  logger.info(req.method + req.url + ' with ip : %s ' + req.ip);
  next();
});
app.use('/', api);
app.use('/api', api);
app.use('/api/static', express.static(path.join(process.cwd(), '/public')));
app.use('/api/users', users);
app.use('/api/tasks', tasks);
app.use('/api/memos', memos);
app.use('/api/auth', auth);
app.use('/api/test', test);
app.use('/api/file', file);

app.use((req, res, next) => {
  res.status(404);
  logger.debug('%s %d %s', req.method, res.statusCode, req.url);
  res.json({ error: 'Not found' });
  return;
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  logger.error('%s %d %s', req.method, res.statusCode, err.message);
  res.json({ error: err.message });
  return;
});

scheduleCronstyle();

module.exports = app;
