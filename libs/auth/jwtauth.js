import url from 'url';
import jwt from 'jwt-simple';

import AccessToken from '../model/accessToken';
import jwtKey from '../../constants/jwtKey';

const jwtauth = (req, res, next) => {
  const parsed_url = url.parse(req.url, true);
  const token = req.headers["x-access-token"] || (req.body && req.body.access_token) || parsed_url.query.access_token;
  if (token) {
    try {
      const decoded = jwt.decode(token, jwtKey);
      // if (decoded.exp <= Date.now()) {
      //     res.end('Access token has expired', 400)
      // }
      AccessToken.findOne({ 'username': decoded.iss }, (err, user) => {
        return (!err && user) ? next() : res.end('Access token has expired', 400);
      })
    }
    catch (err) {
      res.end('Access token has expired', 400)
    }
  }
  else {
    res.end('Access token has expired', 400)
  }
};
export default jwtauth;