# REST API SERVER

restful api server  提供以json为主的api，提供文件、json数据服务。

### url

* <site>/api/  **GET** 暂时无意义
* <site>/api/memos
  * <site>/api/memos/public **GET** **POST**  公有消息
  * <site>/api/memos/public:id **DELETE** 公有消息
* <site>/api/static/
  * <site>/api/static/<file-url> **GET** 资源获取（注意此处是url,不能是uri）
* <site>/api/test
* <site>/api/auth   **POST** 用户登陆
* <site>/api/task  **GET** 获取task列表
  * /api/task/<task-id> **GET POST DELETE** 获取、创建、删除task及相关comments
  * /api/task/<task-id>/comments **POST** 创建comment
    * <site>/api/task/<task-id>/comments/<comment-id> **DELETE ** 删除comment


### datebase

使用mongodb

数据库名：apiDB

**现有以下集合：**

AccessToken

| username | password |grade | name   |
|:--------:|:--------:|:----:|:------:|
| String   | String   |Number| String |


PublicMemo

|     id     |  title   |  grade | content   |  date     |   author  |
|:----------:|:--------:|:------:|:---------:|:---------:|:---------:|
|  ObjectId  | String   |  Number|  String   |  String   |  String   |

PrivateMemo


### log
使用winston 日志包管理系统

现有以下日志分类
```
error   （红色） 错误的请求
warn    （黄色） 系统的开启（意味着宕机）、数据库开启
info    （绿色） 对api的请求
verbose （蓝色）对数据库修改操作（不包含读取）
```
### backup 
日志备份功能
每天UTC（0时区）20时自动发送日志到指定邮箱

## Modules used

* [express](https://github.com/expressjs/express) - 服务器框架
* [jwt-simple](https://github.com/hokaccha/node-jwt-simple) - token验证工具
* [mongoose](https://github.com/Automattic/mongoose) -mongodb的nodejs包
* [winston](https://github.com/winstonjs/winston) - 日志管理系统

## Tools used

[httpie](https://github.com/jkbr/httpie) - GET、POST等请求测使用工具

## Author

[Coul Turing](https://github.com/CoulDracula)

coulturing@gmail.com


## License

[MIT](https://github.com/ealeksandrov/NodeAPI/blob/master/LICENSE)
