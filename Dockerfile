FROM node

MAINTAINER coul 'coulturing@gmail.com'

WORKDIR /app

COPY ./package.json /app/

RUN npm install

COPY . /app/

EXPOSE 8000

CMD npm start -s